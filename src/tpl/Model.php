<?php

namespace app\admin\model;

use think\model\concern\SoftDelete;

/** 
[property]
 */
class ModelName extends \think\Model
{
    use SoftDelete;

    protected $table = '[table]';
    
    //relations
}
