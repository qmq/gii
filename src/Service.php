<?php

namespace qmq\tp6;

use think\facade\Route;

class Service extends \think\Service
{
    public function boot()
    {
        define("GII_VIEW_PATH", __DIR__ . "/view/gii.html");

        $this->commands([
            'qmq:model' => command\Model::class
        ]);
        Route::post('gii/model', "\\qmq\\tp6\\controller\\Gii@model");
        Route::get('gii', "\\qmq\\tp6\\controller\\Gii@index");
    }
}
